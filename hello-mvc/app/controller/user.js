
app.controller('userController', function($scope,appService,$routeParams) {
$scope.fName = '';
$scope.lName = '';
$scope.city = '';
$scope.address = '';
$scope.phone = '';
$scope.passw1 = '';
$scope.passw2 = '';
$scope.users = appService.getUserList();
$scope.edit = true;
$scope.error = false;
$scope.incomplete = false;

  if ($routeParams.id != null) {
    $scope.id=$routeParams.id;
    $scope.addEditHeading="Edit User";
    $scope.edit = false;
    $scope.fName = $scope.users[$routeParams.id-1].fName;
    $scope.lName = $scope.users[$routeParams.id-1].lName;
    $scope.city = $scope.users[$routeParams.id-1].city;
    $scope.address = $scope.users[$routeParams.id-1].address;
    $scope.phone = $scope.users[$routeParams.id-1].phone;  
    $scope.passw1 = $scope.users[$routeParams.id-1].passw1; 
    $scope.passw2 = $scope.users[$routeParams.id-1].passw2;
  } else {
      $scope.addEditHeading="Add New User";
  }
$scope.goAddUserView=function(){
  window.location="#/add-user";
}
$scope.goEditUser = function(id) {
  window.location="#/edit-user/"+id;
};

$scope.editUser = function(id) {
  if (id == 'new') {
    $scope.edit = true;
    $scope.incomplete = true;
    $scope.fName = '';
    $scope.lName = '';
    $scope.city = '';
    $scope.address = '';
    $scope.phone = '';
    $scope.passw1='';
    $scope.passw2='';
    } else {
    $scope.edit = false;
    $scope.fName = $scope.users[id-1].fName;
    $scope.lName = $scope.users[id-1].lName;
    $scope.city = $scope.users[id-1].city;
    $scope.address = $scope.users[id-1].address;
    $scope.phone = $scope.users[id-1].phone;
    $scope.passw1=$scope.users[id-1].passw1;
    $scope.passw2=$scope.users[id-1].passw2
  }
};

$scope.goListView=function(){
  window.location="#/list-user";
};
$scope.addEditUser=function(){
  console.log("array-length:"+$scope.users.length);
  if($scope.id==null){ //New user 
      $scope.users[$scope.users.length]={
      id:$scope.users.length+1,
      fName:$scope.fName,
      lName:$scope.lName,
      city:$scope.city,
      address:$scope.address,
      phone:$scope.phone,
      password:$scope.passw1,
  
    };
  }else{ //update user
    var user=new Object();
    user.id=$scope.id;
    user.fName=$scope.fName;
    user.lName=$scope.lName;
    user.city=$scope.city;
    user.address=$scope.address;
    user.phone=$scope.phone;
    user.password=$scope.passw1;
    appService.updateUser(user);
  }
  appService.setUserList($scope.users);
  window.location="#/list-user";
  //console.log("array:"+JSON.stringify($scope.users));
};

$scope.copyUser=function(){
  console.log('array-length>>>'+$scope.users.length);

    $scope.users[$scope.users.length]={
    id:$scope.users.length+1,
    fName:$scope.fName,
    lName:$scope.lName,
    city:$scope.city,
    address:$scope.address,
    phone:$scope.phone,
    password:$scope.passw1 
  };
 // console.log("scope.users>",$scope.users);
  appService.setUserList($scope.users);
  window.location="#/list-user";
};

$scope.$watch('passw1',function() {$scope.test();});
$scope.$watch('passw2',function() {$scope.test();});
$scope.$watch('fName', function() {$scope.test();});
$scope.$watch('lName', function() {$scope.test();});
$scope.$watch('city', function() {$scope.test();});
$scope.$watch('address', function() {$scope.test();});
$scope.$watch('phone', function() {$scope.test();});


$scope.test = function() {
  if ($scope.passw1 !== $scope.passw2) {
    $scope.error = true;
    } else {
    $scope.error = false;
  }
  $scope.incomplete = false;
  if ($scope.edit && (!$scope.fName.length ||
  !$scope.lName.length ||
  !$scope.passw1.length || !$scope.passw2.length)) {
     $scope.incomplete = true;
  }
};
});