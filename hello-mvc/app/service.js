/* Generic Services */                                                                                                                                                                                                    
 angular.module('app.service', [])                                                                                                                                                                        
   .factory("appService", function($http) {
   	 var user = null;
     var userList = [
    {id:1, fName:'Hege', lName:"Pege",city:"veraval",address:"vrl colony,veraval",phone:"2323223", password:"343446"},
    {id:2, fName:'Kim',  lName:"Pim",city:"bhavnagar",address:"jagatpur amd",phone:"3353424", password:"343454" },
    {id:3, fName:'Sal',  lName:"Smith",city:"ahmedabad",address:"chandlodiya amd",phone:"5344243", password:"43434" },
    {id:4, fName:'Jack', lName:"Jones", city:"junagadh",address:"chanakya puri amd",phone:"3342423",password:"343424" },
    {id:5, fName:'John', lName:"Doe",city:"rajkot",address:"ghatlodiya road",phone:"34453424", password:"343432" },
    {id:6, fName:'Peter',lName:"Pan",city:"una",address:"vastrapur road",phone:"3444223", password:"23234" }
    ];                                                                                                                                                
     return {                                                                                                                                                                                                              
        doSomething: function() {
       	  console.log("doSomething");
        },
        setUserList: function(list) {
          userList=list;
        },
        getUserList: function() {
          return userList;
        },        
        updateUser: function(currentUser) {
          if(currentUser!=null && currentUser.id!=null){
            // list is started from index 0
            var listIndex=currentUser.id-1;
           // console.log("check:"+JSON.stringify(userList[listIndex]));
            userList[listIndex].fName=currentUser.fName;
            userList[listIndex].lName=currentUser.lName;
            userList[listIndex].city=currentUser.city;
            userList[listIndex].address=currentUser.address;
            userList[listIndex].phone=currentUser.phone;
            userList[listIndex].password=currentUser.password;

          }
        }
    }
});