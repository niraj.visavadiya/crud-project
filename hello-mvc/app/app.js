var app = angular.module('myApp', ['ngRoute','app.service']);

app.config([ '$routeProvider','$locationProvider',
	function($routeProvider, $locationProvider) {
		$routeProvider.when('/aboutus', {
         templateUrl : './app/views/about.html',
         controller : 'aboutController'
		}).when('/user', {
         templateUrl : './app/views/user.html',
         controller : 'userController'
		}).when('/list-user', {
         templateUrl : './app/views/list-user.html',
         controller : 'userController'
		}).when('/add-user', {
         templateUrl : './app/views/add-user.html',
         controller : 'userController'
		}).when('/edit-user/:id', {
         templateUrl : './app/views/add-user.html',
         controller : 'userController'
		}).when('/main', {
         templateUrl : './app/views/main.html',
         controller : 'mainController'
		}).otherwise({
			redirectTo : '/list-user'
		});

	}
]);
